$(function() 
    	{
  			$('[data-toggle="tooltip"]').tooltip();
  			$('[data-toggle="popover"]').popover();
  			$('.carousel').carousel({
  				interval: 2000
  			});

        $('#modal-reserva').on('show.bs.modal',function(e)
        {
          console.log("El modal se esta mostrando");
          $('#hotel1-modal').removeClass('btn-outline-success');
          $('#hotel1-modal').addClass('btn-secondary');          
          $('#hotel1-modal').prop('disabled',true);   
        });

        $('#modal-reserva').on('shown.bs.modal',function(e)
        {
          console.log("El modal se ha mostrado");
        });

        $('#modal-reserva').on('hide.bs.modal',function(e)
        {
          console.log("El modal se esta ocultando");
        });

        $('#modal-reserva').on('hidden.bs.modal',function(e)
        {
          console.log("El modal se ha ocultado");
          $('#hotel1-modal').prop('disabled',false);  
          $('#hotel1-modal').removeClass('btn-secondary');
          $('#hotel1-modal').addClass('btn-outline-success');
        });
		});